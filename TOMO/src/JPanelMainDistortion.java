import ij.ImagePlus;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by thom1 on 11/05/2017.
 */
public class JPanelMainDistortion extends JPanel implements JPanelMain {

    private JPanelImage image;
    private JPanelInfoDistortion info;

    public JPanelMainDistortion(String text) {
        super(new BorderLayout());
        int imageIndex = Frame.getInstance().getConfig().getDistortionMin();
        this.image = new JPanelImage(JsonController.loadSdnr(), text, imageIndex);
        this.info = new JPanelInfoDistortion(text);
        image.setJPanelMain(this);
        this.add(image, BorderLayout.CENTER);
        this.add(info, BorderLayout.EAST);
    }


    @Override
    public void calcul(ArrayList<Forme> formes) {

    }
}
