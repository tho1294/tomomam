import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by thom1 on 01/05/2017.
 */
public class ConfigListener implements ActionListener {

    private Config config;

    private TextField sdnr;
    private TextField asfMin;
    private TextField asfMax;
    private TextField homogeneity;
    private TextField concordance;
    private TextField ADiagonal;
    private TextField BDiagonal;
    private TextField CDiagonal;
    private TextField distortionMin;
    private TextField distortionMax;

    public ConfigListener(Config config) {
        this.config = config;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JPanel panel = new JPanel();
        sdnr = new TextField();
        asfMin = new TextField();
        asfMax = new TextField();
        homogeneity = new TextField();
        concordance = new TextField();
        ADiagonal = new TextField();
        BDiagonal = new TextField();
        CDiagonal = new TextField();
        distortionMin = new TextField();
        distortionMax = new TextField();

        if (config != null) {
            sdnr.setText(config.getSdnr() + "");
            asfMin.setText(config.getAsfMin() + "");
            asfMax.setText(config.getAsfMax() + "");
            homogeneity.setText(config.getHomogeneity() + "");
            concordance.setText(config.getConcordance() + "");
            ADiagonal.setText(config.getADiagonal() + "");
            BDiagonal.setText(config.getBDiagonal() + "");
            CDiagonal.setText(config.getCDiagonal() + "");
            distortionMin.setText(config.getDistortionMin() + "");
            distortionMax.setText(config.getDistortionMax() + "");
        }

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(panelSdnr());
        panel.add(panelAsf());
        panel.add(panelHomogeneity());
        panel.add(panelConcordance());
        panel.add(panelDistortion());

        JOptionPane d = new JOptionPane();
        int retour = d.showConfirmDialog(Frame.getInstance(), panel, "Configuration", JOptionPane.OK_CANCEL_OPTION);

        if (retour == JOptionPane.OK_OPTION) {
            int homogeneityV = Tools.StringToInteger(homogeneity.getText());
            int asfMinV = Tools.StringToInteger(asfMin.getText());
            int asfMaxV = Tools.StringToInteger(asfMax.getText());
            int sdnrV = Tools.StringToInteger(sdnr.getText());

            int concordanceV = Tools.StringToInteger(concordance.getText());
            float ADiagonalV = Tools.StringToFloat(ADiagonal.getText());
            float BDiagonalV = Tools.StringToFloat(BDiagonal.getText());
            float CDiagonalV = Tools.StringToFloat(CDiagonal.getText());

            int distortionMinV = Tools.StringToInteger(distortionMin.getText());
            int distortionMaxV = Tools.StringToInteger(distortionMax.getText());

            Config newConfig = new Config(homogeneityV, asfMinV, asfMaxV, sdnrV,
                    concordanceV, ADiagonalV, BDiagonalV, CDiagonalV,
                    distortionMinV, distortionMaxV);
            this.config = newConfig;
            Frame.getInstance().setConfig(newConfig);
            JsonController.saveConfig(newConfig);
        }
    }

    private JPanel panelSdnr() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(title("SDNR"));
        JPanel content = new JPanel(new GridLayout(1, 2));
        content.add(new JLabel("couche"));
        content.add(sdnr);
        panel.add(content);
        panel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        return panel;
    }

    private JPanel panelHomogeneity() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(title("HOMOGENEITY"));
        JPanel content = new JPanel(new GridLayout(1, 2));
        content.add(new JLabel("couche"));
        content.add(homogeneity);
        panel.add(content);
        panel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        return panel;
    }

    private JPanel panelAsf() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(title("ASF"));

        JPanel content = new JPanel(new GridLayout(2, 2));
        content.add(new JLabel("couche Min"));
        content.add(asfMin);
        content.add(new JLabel("couche Max"));
        content.add(asfMax);
        panel.add(content);
        panel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        return panel;
    }

    private JPanel panelConcordance() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(title("CONCORDANCE"));

        JPanel content = new JPanel(new GridLayout(4, 2));
        content.add(new JLabel("couche"));
        content.add(concordance);

        content.add(new JLabel("diagonal A"));
        content.add(ADiagonal);

        content.add(new JLabel("diagonal B"));
        content.add(BDiagonal);

        content.add(new JLabel("diagonal C"));
        content.add(CDiagonal);

        panel.add(content);
        panel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        return panel;
    }

    private JPanel panelDistortion() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(title("DISTORTION"));

        JPanel content = new JPanel(new GridLayout(2, 2));
        content.add(new JLabel("couche min"));
        content.add(distortionMin);

        content.add(new JLabel("couche max"));
        content.add(distortionMax);

        panel.add(content);
        panel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        return panel;
    }


    private JPanel title(String text) {
        JPanel title = new JPanel(new FlowLayout(FlowLayout.CENTER));
        title.add(new JLabel(text));
        return title;
    }
}
