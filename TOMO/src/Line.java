import java.awt.*;

/**
 * Created by thom1 on 06/05/2017.
 */
public class Line extends Forme {

    private Point A;
    private Point B;

    private boolean pointA;
    private boolean pointB;

    public Line(String text, int borderSize, Color color, Point A, Point B) {
        super(text, borderSize, color, A.x, A.y, B.x, B.y);
        this.A = A;
        this.B = B;

    }


    public void mouseReleased() {
        pointA = false;
        pointB = false;
    }

    public void mousePressed(Point p, float zoom) {
        double distanceA = Math.sqrt(Math.pow((A.x * zoom - p.x), 2) + Math.pow((A.y * zoom - p.y), 2));
        double distanceB = Math.sqrt(Math.pow((B.x * zoom - p.x), 2) + Math.pow((B.y * zoom - p.y), 2));
        if (distanceA <= 10) {
            pointA = true;
        } else if (distanceB <= 10) {
            pointB = true;
        }

    }

    public boolean mouseDragged(Point p, float zoom) {
        boolean result = false;
        if (pointA) {
            A.x = (int) (p.x / zoom);
            A.y = (int) (p.y / zoom);
            result = true;
        } else if (pointB) {
            B.x = (int) (p.x / zoom);
            B.y = (int) (p.y / zoom);
            result = true;
        }

        return result;
    }


    public void draw(Graphics2D g, float zoom) {
        Stroke newStroke = new BasicStroke(borderSize * 2 * zoom);
        Stroke oldStroke = g.getStroke();
        Color oldColor = g.getColor();
        g.setColor(color);
        g.setStroke(newStroke);
        g.drawString(text, (A.x + 10) * zoom, A.y * zoom + 10);
        int ovalW = 10;
        int ovalH = 10;
        g.fillOval((int) (A.x * zoom - ovalW / 2), (int) (A.y * zoom - ovalH / 2), ovalW, ovalH);
        g.fillOval((int) (B.x * zoom - ovalW / 2), (int) (B.y * zoom - ovalH / 2), ovalW, ovalH);
        g.drawLine((int) (A.x * zoom), (int) (A.y * zoom), (int) (B.x * zoom), (int) (B.y * zoom));
        g.setStroke(oldStroke);
        g.setColor(oldColor);
    }

    public Point getA() {
        return A;
    }

    public Point getB() {
        return B;
    }
}
