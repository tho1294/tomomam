import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by thom1 on 26/04/2017.
 */
public class Calcul {

    public static float max(ArrayList<Pixel> pixels) {
        float max = Float.MIN_VALUE;
        for (int i = 0; i < pixels.size(); i++) {
            if (pixels.get(i).getValue() > max) {
                max = pixels.get(i).getValue();
            }
        }
        return max;
    }

    public static float moyenne(ArrayList<Pixel> pixels) {
        float moy = 0;
        for (int i = 0; i < pixels.size(); i++) {
            moy += pixels.get(i).getValue();
        }
        return moy / pixels.size();
    }


    public static float variance(ArrayList<Pixel> pixels) {
        float moyenne = moyenne(pixels);
        float temp = 0;
        for (int i = 0; i < pixels.size(); i++) {
            temp += (pixels.get(i).getValue() - moyenne) * (pixels.get(i).getValue() - moyenne);
        }
        return temp / pixels.size();
    }

    public static float StdDev(ArrayList<Pixel> pixels) {
        return (float) Math.sqrt(variance(pixels));
    }

    public static float median(ArrayList<Pixel> pixels) {
        ArrayList<Pixel> pixelT = new ArrayList<>(pixels);
        Collections.sort(pixelT);

        if (pixelT.size() % 2 == 0) {
            return (pixelT.get((pixelT.size() / 2) - 1).getValue() + pixelT.get(pixelT.size() / 2).getValue()) / 2.0f;
        }
        return pixelT.get(pixelT.size() / 2).getValue();
    }
}
