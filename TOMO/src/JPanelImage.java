import ij.ImagePlus;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by thom1 on 01/05/2017.
 */
public class JPanelImage extends JPanel {
    private JScrollPaneImage jScrollPaneImage;
    private JPanelMenuImage jPanelMenuImage;
    private ArrayList<Forme> formes;
    private String text;
    private JPanelMain jPanelMain;

    public JPanelImage(ArrayList<Forme> formes, String text, int imageIndex) {
        super();
        this.setLayout(new BorderLayout());
        this.formes = formes;
        this.text = text;

        this.jScrollPaneImage = new JScrollPaneImage(formes, imageIndex);
        this.jPanelMenuImage = new JPanelMenuImage(this, imageIndex);
        this.add(jScrollPaneImage, BorderLayout.CENTER);
        this.add(jPanelMenuImage, BorderLayout.SOUTH);
    }

    public void save() {
        JsonController.saveFormes(formes, text);
    }

    public void calcul() {
        jPanelMain.calcul(formes);
    }

    public void setImage(int index) {
        jScrollPaneImage.setImage(index);
    }


    public void setJPanelMain(JPanelMain jPanelMain) {
        this.jPanelMain = jPanelMain;
    }
}
