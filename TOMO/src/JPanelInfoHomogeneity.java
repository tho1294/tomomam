import javax.swing.*;
import java.awt.*;

/**
 * Created by thom1 on 06/05/2017.
 */
public class JPanelInfoHomogeneity extends JPanelInfo {

    private JLabel MEAN_REF;
    private JLabel MEAN_A;
    private JLabel MEAN_B;
    private JLabel MEAN_C;

    private JLabel RATIO_A;
    private JLabel RATIO_B;
    private JLabel RATIO_C;

    private JLabel MEAN_RATIO;

    private JLabel DEVIATION;

    public JPanelInfoHomogeneity(String text) {
        super(text);

        add(title("Homogeneity Roi"));
        add(meanPixelRoi());
        add(ratioRef());
        add(result());
    }


    private JPanel title(String title) {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        panel.add(new JLabel(title));
        return panel;
    }

    private JPanel meanPixelRoi() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JPanel title = new JPanel(new FlowLayout(FlowLayout.CENTER));
        title.add(new JLabel("MEAN PIXELS ROI"));

        JPanel ref = new JPanel(new FlowLayout(FlowLayout.LEFT));
        ref.add(new JLabel("Ref"));
        MEAN_REF = new JLabel(" ");
        ref.add(MEAN_REF);
        ref.add(MEAN_REF);

        JPanel zoneA = new JPanel(new FlowLayout(FlowLayout.LEFT));
        zoneA.add(new JLabel("Zone A"));
        MEAN_A = new JLabel(" ");
        zoneA.add(MEAN_A);

        JPanel zoneB = new JPanel(new FlowLayout(FlowLayout.LEFT));
        zoneB.add(new JLabel("Zone B"));
        MEAN_B = new JLabel(" ");
        zoneB.add(MEAN_B);

        JPanel zoneC = new JPanel(new FlowLayout(FlowLayout.LEFT));
        zoneC.add(new JLabel("Zone C"));
        MEAN_C = new JLabel(" ");
        zoneC.add(MEAN_C);

        panel.add(title);
        panel.add(ref);
        panel.add(zoneA);
        panel.add(zoneB);
        panel.add(zoneC);
        panel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        return panel;
    }

    private JPanel ratioRef() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JPanel title = new JPanel(new FlowLayout(FlowLayout.CENTER));
        title.add(new JLabel("RATIO  ROI REF PERIPHERY"));

        JPanel zoneA = new JPanel(new FlowLayout(FlowLayout.LEFT));
        zoneA.add(new JLabel("Ratio A"));
        RATIO_A = new JLabel(" ");
        zoneA.add(RATIO_A);

        JPanel zoneB = new JPanel(new FlowLayout(FlowLayout.LEFT));
        zoneB.add(new JLabel("Ratio B"));
        RATIO_B = new JLabel(" ");
        zoneB.add(RATIO_B);

        JPanel zoneC = new JPanel(new FlowLayout(FlowLayout.LEFT));
        zoneC.add(new JLabel("Ratio C"));
        RATIO_C = new JLabel(" ");
        zoneC.add(RATIO_C);

        panel.add(title);
        panel.add(zoneA);
        panel.add(zoneB);
        panel.add(zoneC);
        panel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        return panel;
    }

    private JPanel result() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JPanel mean = new JPanel(new FlowLayout(FlowLayout.LEFT));
        mean.add(new JLabel("MEAN"));
        MEAN_RATIO = new JLabel(" ");
        mean.add(MEAN_RATIO);

        JPanel deviation = new JPanel(new FlowLayout(FlowLayout.LEFT));
        deviation.add(new JLabel("DEVIATION IN %"));
        DEVIATION = new JLabel(" ");
        deviation.add(DEVIATION);

        panel.add(mean);
        panel.add(deviation);
        panel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        return panel;
    }

    public void setInfo(Homogeneity info) {
        this.information = info;

        MEAN_REF.setText(info.getMEAN_REF() + "");
        MEAN_A.setText(info.getMEAN_A() + "");
        MEAN_B.setText(info.getMEAN_B() + "");
        MEAN_C.setText(info.getMEAN_C() + "");

        RATIO_A.setText(info.getRATIO_A() + "");
        RATIO_B.setText(info.getRATIO_B() + "");
        RATIO_C.setText(info.getRATIO_C() + "");

        MEAN_RATIO.setText(info.getMEAN_RATIO() + "");

        DEVIATION.setText(info.getDEVIATION() + "");

    }
}
