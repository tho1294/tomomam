/**
 * Created by thom1 on 26/04/2017.
 */
public class Pixel implements Comparable<Pixel> {

    private float value;
    private int x;
    private int y;

    public Pixel(int value, int x, int y) {
        this.value = value;
        this.x = x;
        this.y = y;
    }

    public float getValue() {
        return value;
    }

    @Override
    public int compareTo(Pixel o) {
        return (int) (this.value - o.getValue());
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
