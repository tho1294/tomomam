import com.lowagie.text.Document;
import ij.ImagePlus;

/**
 * Created by thom1 on 06/05/2017.
 */
public class Homogeneity implements  Information{

    private float MEAN_REF;
    private float MEAN_A;
    private float MEAN_B;
    private float MEAN_C;

    private float RATIO_A;
    private float RATIO_B;
    private float RATIO_C;

    private float MEAN_RATIO;

    private float DEVIATION;

    private Rect ref;
    private Rect A;
    private Rect B;
    private Rect C;
    private ImagePlus image;

    public Homogeneity(ImagePlus image, Rect ref, Rect A, Rect B, Rect C) {
        this.image = image;
        this.ref = ref;
        this.A = A;
        this.B = B;
        this.C = C;
        calcul();
    }

    private void calcul() {
        MEAN_REF = Calcul.moyenne(Tools.getPixelFromRect(ref, image));
        MEAN_A = Calcul.moyenne(Tools.getPixelFromRect(A, image));
        MEAN_B = Calcul.moyenne(Tools.getPixelFromRect(B, image));
        MEAN_C = Calcul.moyenne(Tools.getPixelFromRect(C, image));

        RATIO_A = MEAN_A / MEAN_REF;
        RATIO_B = MEAN_B / MEAN_REF;
        RATIO_C = MEAN_C / MEAN_REF;

        MEAN_RATIO = (RATIO_A + RATIO_B + RATIO_C) / 3;

        float max = Math.max(RATIO_A, RATIO_B);
        max = Math.max(max, RATIO_C);

        float min = Math.min(RATIO_A, RATIO_B);
        min = Math.min(min, RATIO_C);


        DEVIATION = ((max - min) / MEAN_RATIO) * 100;
    }

    public float getMEAN_REF() {
        return MEAN_REF;
    }

    public float getMEAN_A() {
        return MEAN_A;
    }

    public float getMEAN_B() {
        return MEAN_B;
    }

    public float getMEAN_C() {
        return MEAN_C;
    }

    public float getRATIO_A() {
        return RATIO_A;
    }

    public float getRATIO_B() {
        return RATIO_B;
    }

    public float getRATIO_C() {
        return RATIO_C;
    }

    public float getMEAN_RATIO() {
        return MEAN_RATIO;
    }

    public float getDEVIATION() {
        return DEVIATION;
    }

    @Override
    public void exportation(Document document) {

    }
}
