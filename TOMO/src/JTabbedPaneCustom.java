import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Created by thom1 on 01/05/2017.
 */
public class JTabbedPaneCustom extends JTabbedPane implements ChangeListener {

    private JPanelMainSDNR sdnr;
    private JPanelMainASF asf;
    private JPanelMainHomogeneity homogeneity;
    private JPanelMainConcordance concordance;
    private JPanelMainDistortion distortion;

    public JTabbedPaneCustom() {
        super();
        this.addChangeListener(this);

        sdnr = new JPanelMainSDNR("sdnr");
        asf = new JPanelMainASF("asf");
        homogeneity = new JPanelMainHomogeneity("homogeneity");
        concordance = new JPanelMainConcordance("concordance");
        distortion = new JPanelMainDistortion("distortion");

        this.addTab("SDNR", sdnr);
        this.addTab("asf", asf);
        this.addTab("homogeneity", homogeneity);
        this.addTab("concordance", concordance);
        this.addTab("distortion", distortion);
    }

    @Override
    public void stateChanged(ChangeEvent e) {

    }
}
