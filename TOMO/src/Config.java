import java.awt.*;

/**
 * Created by thom1 on 20/03/2017.
 */
public class Config {

    private int homogeneity;
    private int asfMin;
    private int asfMax;
    private int sdnr;

    private int concordance;
    private float ADiagonal;
    private float BDiagonal;
    private float CDiagonal;

    private int distortionMin;
    private int distortionMax;


    public Config(int homogeneity, int asfMin, int asfMax, int sdnr, int concordance, float ADiagonal, float BDiagonal, float CDiagonal, int ditortionMin, int distortionMax) {
        this.homogeneity = homogeneity;
        this.asfMin = asfMin;
        this.asfMax = asfMax;
        this.sdnr = sdnr;
        this.concordance = concordance;
        this.ADiagonal = ADiagonal;
        this.BDiagonal = BDiagonal;
        this.CDiagonal = CDiagonal;
        this.distortionMin = ditortionMin;
        this.distortionMax = distortionMax;
    }

    public int getHomogeneity() {
        return homogeneity;
    }

    public int getAsfMin() {
        return asfMin;
    }

    public int getAsfMax() {
        return asfMax;
    }

    public int getSdnr() {
        return sdnr;
    }

    public int getConcordance() {
        return concordance;
    }

    public float getADiagonal() {
        return ADiagonal;
    }

    public float getBDiagonal() {
        return BDiagonal;
    }

    public float getCDiagonal() {
        return CDiagonal;
    }

    public int getDistortionMin() {
        return distortionMin;
    }

    public int getDistortionMax() {
        return distortionMax;
    }
}
