import ij.ImagePlus;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by thom1 on 04/05/2017.
 */
public class JPanelMainASF extends JPanel implements JPanelMain {
    private JPanelImage image;
    private JPanelInfoASF info;

    public JPanelMainASF(String text) {
        super(new BorderLayout());
        int imageIndex = Frame.getInstance().getConfig().getAsfMin();
        this.image = new JPanelImage(JsonController.loadAsF(), text, imageIndex);
        this.info = new JPanelInfoASF(text);
        image.setJPanelMain(this);
        this.add(image, BorderLayout.CENTER);
        this.add(info, BorderLayout.EAST);
    }

    @Override
    public void calcul(ArrayList<Forme> formes) {
        Rect zone = null;
        Rect background = null;


        for (int i = 0; i < formes.size(); i++) {
            if (formes.get(i).getText().equals("zone")) {
                zone = (Rect) formes.get(i);
            } else if (formes.get(i).getText().equals("background")) {
                background = (Rect) formes.get(i);
            }
        }

        Config config = Frame.getInstance().getConfig();
        ArrayList<ImagePlus> images = new ArrayList<>();

        for (int i = config.getAsfMin(); i < config.getAsfMax(); i++) {
            images.add(Frame.getInstance().getImage(i));
        }
        ASF asf = new ASF(images, zone, background);
        info.setInfo(asf);
        revalidate();
    }
}
