import ij.ImagePlus;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by thom1 on 02/05/2017.
 */
public class JScrollPaneImage extends JScrollPane {

    JPanelComponentImage jPanelComponentImage;

    public JScrollPaneImage(ArrayList<Forme> formes, int imageIndex) {
        super(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        jPanelComponentImage = new JPanelComponentImage(this, formes,imageIndex);
        this.setViewportView(jPanelComponentImage);
    }

    public void setImage(int index) {
        jPanelComponentImage.setImage(index);
    }
}
