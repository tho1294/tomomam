import java.awt.*;

import static ij.plugin.PointToolOptions.update;

/**
 * Created by thom1 on 07/03/2017.
 */
public class Circle extends Forme{

    private boolean center;
    private boolean border;

    public Circle(String text, int borderSize, Color color, int x, int y, int width) {
        super(text,borderSize,color,x,y,x+width,y+width);
    }

    public void mouseReleased() {
        border = false;
        center = false;
    }

    public void mousePressed(Point p, float zoom) {
        double distance = Math.sqrt(Math.pow((x * zoom - p.x), 2) + Math.pow((y * zoom - p.y), 2));
        double circleP = (width * zoom / 2);
        if (distance <= circleP + borderSize / 2 * zoom && distance >= circleP - borderSize / 2 * zoom) {
            border = true;
        } else if (distance <= circleP - borderSize / 2 * zoom) {
            center = true;
        }
    }

    public boolean mouseDragged(Point p, float zoom) {
        boolean result = false;
        if (border) {
            double distance = Math.sqrt(Math.pow((x * zoom - p.x), 2) + Math.pow((y * zoom - p.y), 2));
            width = (int) (distance*2/zoom);
            result = true;
        }

        if (center == true) {
            x = (int)(p.x/zoom);
            y = (int)(p.y/zoom);
            result = true;
        }
        return result;
    }



    public void draw(Graphics2D g, float zoom) {
        Stroke newStroke = new BasicStroke(borderSize * 2 * zoom);
        Stroke oldStroke = g.getStroke();
        Color oldColor = g.getColor();
        g.setColor(color);
        g.setStroke(newStroke);
        g.drawString(text, (x + width + 10) * zoom, y * zoom + 10);
        g.drawOval((int) ((x - width / 2) * zoom), (int) ((y - width / 2) * zoom), (int) (width * zoom), (int) (width * zoom));

        g.setStroke(oldStroke);
        g.setColor(oldColor);
    }

}
