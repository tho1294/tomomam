import ij.ImagePlus;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by thom1 on 01/05/2017.
 */
public class Frame extends JFrame {
    private static Frame INSTANCE = new Frame();
    private File[] files;
    private ImagePlus[] images;
    private Config config;
    private JTabbedPaneCustom tabbedPaneCustom;

    private Frame() {
        super();
        config = JsonController.loadConfig();
        initialisation();
    }

    public static Frame getInstance() {
        return INSTANCE;
    }

    private void initialisation() {
        initFrame();
        this.setJMenuBar(initMenu());
        this.revalidate();
    }


    private void initFrame() {
        this.setTitle("TOMOSOFT ");
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int) (screenSize.getWidth() / 1.3);
        int height = (int) (screenSize.getHeight() / 1.3);
        this.setSize(width, height);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    private JMenuBar initMenu() {
        JMenuBar menuBar = new JMenuBar();

        JMenu fichier = new JMenu("Fichier");
        menuBar.add(fichier);
        JMenuItem ouvrir = new JMenuItem("ouvrir");
        fichier.add(ouvrir);
        ouvrir.addActionListener(new OpenListener(this));

        JMenu configuration = new JMenu("Configuration");

        JMenuItem config = new JMenuItem("configuration");
        configuration.add(config);
        config.addActionListener(new ConfigListener(this.config));

        menuBar.add(configuration);

        return menuBar;
    }

    public void setFiles(File[] files) {
        this.files = files;
        this.images = new ImagePlus[files.length];
        this.tabbedPaneCustom = new JTabbedPaneCustom();
        this.add(tabbedPaneCustom);
        this.revalidate();
    }

    public void setFiles(ImagePlus[] images) {
        this.images = images;

        this.tabbedPaneCustom = new JTabbedPaneCustom();
        this.add(tabbedPaneCustom);
        this.revalidate();
    }

    public ImagePlus getImage(int index) {
        if (images[index] == null) {
            images[index] = new ImagePlus(files[index].getPath());
        }
        return images[index];
    }

    public ImagePlus rotation(int index) {
        ImagePlus image = new ImagePlus(files[index].getPath());
        if (image.getWidth() < image.getHeight()) {
            int locationX = image.getWidth() / 2;
            int locationY = image.getHeight() / 2;

            int rotationRequired = (int) Math.toRadians(90);
            AffineTransform tx = AffineTransform.getQuadrantRotateInstance(rotationRequired, locationX, locationY);
            AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
            image = new ImagePlus(image.getTitle(), op.filter(image.getBufferedImage(), null));
            System.out.println("test");
        }
        return image;
    }

    public int getNbImage() {
        return images.length;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public Config getConfig() {
        return config;
    }


}
