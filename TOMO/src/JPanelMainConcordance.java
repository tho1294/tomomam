import ij.ImagePlus;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by thom1 on 06/05/2017.
 */
public class JPanelMainConcordance extends JPanel implements JPanelMain {

    private JPanelImage image;
    private JPanelInfoConcordance info;

    public JPanelMainConcordance(String text) {
        super(new BorderLayout());
        int imageIndex = Frame.getInstance().getConfig().getConcordance();
        this.image = new JPanelImage(JsonController.loadConcordance(), text, imageIndex);
        this.info = new JPanelInfoConcordance(text);
        image.setJPanelMain(this);
        this.add(image, BorderLayout.CENTER);
        this.add(info, BorderLayout.EAST);
    }

    @Override
    public void calcul(ArrayList<Forme> formes) {
        Line A = null;
        Line B = null;
        Line C = null;


        for (int i = 0; i < formes.size(); i++) {
            if (formes.get(i).getText().equals("A")) {
                A = (Line) formes.get(i);
            } else if (formes.get(i).getText().equals("B")) {
                B = (Line) formes.get(i);
            } else if (formes.get(i).getText().equals("C")) {
                C = (Line) formes.get(i);
            }
        }
        Config config = Frame.getInstance().getConfig();
        ImagePlus images = Frame.getInstance().getImage(config.getConcordance());


        Concordance concordance = new Concordance(images, A, B, C);
        info.setInfo(concordance);
        revalidate();
    }
}
