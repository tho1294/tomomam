import java.awt.*;

/**
 * Created by thom1 on 07/03/2017.
 */
public class Forme {
    protected int borderSize;
    protected Color color;
    protected String text;
    protected int x;
    protected int y;
    protected int x1;
    protected int y1;
    protected int height;
    protected int width;
    protected String forme;

    public Forme(String text, int borderSize, Color color, int x, int y, int x1, int y1) {
        this.borderSize = borderSize;
        this.color = color;
        this.x = x;
        this.y = y;
        this.x1 = x1;
        this.y1 = y1;
        this.width = x1 - x;
        this.height = y1 - y;
        this.text = text;
    }

    public void draw(Graphics2D g, float zoom) {
    }

    public void mousePressed(Point p, float zoom) {
    }

    public boolean mouseDragged(Point p, float zoom) {
        return false;
    }

    public void mouseReleased() {
    }

    public int getBorderSize() {
        return borderSize;
    }

    public Color getColor() {
        return color;
    }

    public String getText() {
        return text;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public String getForme() {
        return forme;
    }

}
