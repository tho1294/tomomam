import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import sun.misc.IOUtils;

import java.awt.*;
import java.io.*;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by thom1 on 20/03/2017.
 */
public class JsonController {
    public static String configFile = "config.json";
    public static String sdnrFile = "sdnr.json";
    public static String asfFile = "asf.json";
    public static String homogeneityFile = "homogeneity.json";
    public static String concordanceFile = "concordance.json";

    public static ArrayList<Forme> loadSdnr() {
        Gson gson = new Gson();

        ArrayList<Circle> circles = null;
        try {
            FileInputStream is = new FileInputStream(new File(sdnrFile));
            String s = getStringFromInputStream(is);
            Type listType = new TypeToken<ArrayList<Circle>>() {
            }.getType();
            circles = gson.fromJson(s, listType);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList<Forme> formes = new ArrayList<>();
        for (int i = 0; i < circles.size(); i++) {
            formes.add(circles.get(i));
        }
        return formes;
    }

    public static ArrayList<Forme> loadAsF() {
        Gson gson = new Gson();

        ArrayList<Rect> rects = null;
        try {
            FileInputStream is = new FileInputStream(new File(asfFile));
            String s = getStringFromInputStream(is);
            Type listType = new TypeToken<ArrayList<Rect>>() {
            }.getType();
            rects = gson.fromJson(s, listType);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList<Forme> formes = new ArrayList<>();
        for (int i = 0; i < rects.size(); i++) {
            formes.add(rects.get(i));
        }
        return formes;
    }

    public static ArrayList<Forme> loadHomogeneity() {
        Gson gson = new Gson();

        ArrayList<Rect> rects = null;
        try {
            FileInputStream is = new FileInputStream(new File(homogeneityFile));
            String s = getStringFromInputStream(is);
            Type listType = new TypeToken<ArrayList<Rect>>() {
            }.getType();
            rects = gson.fromJson(s, listType);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList<Forme> formes = new ArrayList<>();
        for (int i = 0; i < rects.size(); i++) {
            formes.add(rects.get(i));
        }
        return formes;
    }

    public static ArrayList<Forme> loadConcordance() {
        Gson gson = new Gson();

        ArrayList<Line> lines = null;
        try {
            FileInputStream is = new FileInputStream(new File(concordanceFile));
            String s = getStringFromInputStream(is);
            Type listType = new TypeToken<ArrayList<Line>>() {
            }.getType();
            lines = gson.fromJson(s, listType);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList<Forme> formes = new ArrayList<>();
        for (int i = 0; i < lines.size(); i++) {
            formes.add(lines.get(i));
        }
        return formes;
    }

    public static void saveHomogeneity(ArrayList<Rect> rects) {
        Gson gson = new Gson();
        String json = gson.toJson(rects);
        try {
            FileWriter file = new FileWriter(homogeneityFile);
            file.write(json + "");
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveAsf(ArrayList<Rect> rects) {
        Gson gson = new Gson();
        String json = gson.toJson(rects);
        try {
            FileWriter file = new FileWriter(asfFile);
            file.write(json + "");
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveSdnr(ArrayList<Circle> circles) {
        Gson gson = new Gson();
        String json = gson.toJson(circles);
        try {
            FileWriter file = new FileWriter(sdnrFile);
            file.write(json + "");
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveConcordance(ArrayList<Line> lines) {
        Gson gson = new Gson();
        String json = gson.toJson(lines);
        try {
            FileWriter file = new FileWriter(concordanceFile);
            file.write(json + "");
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Config loadConfig() {
        Gson gson = new Gson();
        Config config = null;
        try {
            FileInputStream is = new FileInputStream(new File(configFile));
            String s = getStringFromInputStream(is);
            config = gson.fromJson(s, Config.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return config;
    }

    public static void saveConfig(Config config) {
        Gson gson = new Gson();
        String json = gson.toJson(config);
        try {
            FileWriter file = new FileWriter(configFile);
            file.write(json + "");
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getStringFromInputStream(InputStream is) {
        String s = "";
        try {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader reader = new BufferedReader(isr);
            String line = null;
            while ((line = reader.readLine()) != null) {
                s += line;
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return s;
    }

    public static void saveFormes(ArrayList<Forme> formes, String text) {
        if (text.equals("sdnr")) {
            ArrayList<Circle> circles = new ArrayList<>();
            for (int i = 0; i < formes.size(); i++) {
                circles.add((Circle) formes.get(i));
            }
            saveSdnr(circles);
        } else if (text.equals("asf")) {
            ArrayList<Rect> rects = new ArrayList<>();
            for (int i = 0; i < formes.size(); i++) {
                rects.add((Rect) formes.get(i));
            }
            saveAsf(rects);
        } else if (text.equals("homogeneity")) {
            ArrayList<Rect> rects = new ArrayList<>();
            for (int i = 0; i < formes.size(); i++) {
                rects.add((Rect) formes.get(i));
            }
            saveHomogeneity(rects);
        }else if(text.equals("concordance")){
            ArrayList<Line> lines = new ArrayList<>();
            for (int i = 0; i < formes.size(); i++) {
                lines.add((Line) formes.get(i));
            }
            saveConcordance(lines);
        }
    }
}
