import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Created by thom1 on 03/03/2017.
 */
class Rect extends Forme {

    private boolean left;
    private boolean right;
    private boolean top;
    private boolean bottom;
    private boolean center;
    private Dimension imageSize;

    private Point clickPoint;

    public Rect(String text, Dimension imageSize, int borderSize, Color color, int x, int y, int x1, int y1) {
        super(text, borderSize, color, x, y, x1, y1);
        this.imageSize = imageSize;
        this.forme = "rect";
    }

    public void updateSize() {
        width = x1 - x;
        height = y1 - y;
    }

    public void draw(Graphics2D g, float zoom) {
        Stroke newStroke = new BasicStroke(borderSize * 2 * zoom);
        Stroke oldStroke = g.getStroke();
        Color oldColor = g.getColor();
        g.setColor(color);
        g.setStroke(newStroke);
        g.drawString(text, (x + width + 10) * zoom, y * zoom + 10);
        g.draw(new Rectangle2D.Double(x * zoom, y * zoom, width * zoom, height * zoom));

        g.setStroke(oldStroke);
        g.setColor(oldColor);
    }

    public void mouseReleased() {
        left = false;
        right = false;
        top = false;
        bottom = false;
        center = false;
    }

    public void mousePressed(Point p, float zoom) {
        clickPoint = p;
        if (p.getX() >= x * zoom - borderSize / 2 * zoom
                && p.getX() <= x * zoom + borderSize / 2 * zoom
                && p.getY() >= y * zoom - borderSize / 2 * zoom
                && p.getY() <= (y + height) * zoom + borderSize / 2 * zoom) {
            left = true;
        }

        if (p.getX() >= x * zoom + width * zoom - borderSize / 2 * zoom
                && p.getX() <= x * zoom + width * zoom + borderSize / 2 * zoom
                && p.getY() >= y * zoom - borderSize / 2 * zoom
                && p.getY() <= (y + height) * zoom + borderSize / 2 * zoom) {
            right = true;
        }

        if (p.getY() >= y * zoom + height * zoom - borderSize / 2 * zoom
                && p.getY() <= y * zoom + height * zoom + borderSize / 2 * zoom
                && p.getX() >= x * zoom - borderSize / 2 * zoom
                && p.getX() <= (x * zoom + width) * zoom + borderSize / 2 * zoom) {
            bottom = true;
        }

        if (p.getY() >= y * zoom - borderSize / 2 * zoom
                && p.getY() <= y * zoom + borderSize / 2 * zoom
                && p.getX() >= x * zoom - borderSize / 2 * zoom
                && p.getX() <= (x + width) * zoom + borderSize / 2 * zoom) {
            top = true;
        }

        if (p.getX() >= x * zoom + borderSize / 2 * zoom
                && p.getX() <= (x + width) * zoom - borderSize / 2 * zoom
                && p.getY() >= y * zoom + borderSize / 2 * zoom
                && p.getY() <= (y + height) * zoom - borderSize / 2 * zoom) {
            center = true;
        }
    }

    public boolean mouseDragged(Point p, float zoom) {
        boolean result = false;
        if (left == true) {
            if ((int) (p.getX() / zoom) > x1) {
                left = false;
                right = true;
            } else if ((int) (p.getX() / zoom) - borderSize / 2 > 0) {

                x = ((int) (p.getX() / zoom));
                updateSize();
            }
            result = true;
        }
        if (right == true) {
            if ((int) (p.getX() / zoom) < x) {
                left = true;
                right = false;
            } else if ((int) (p.getX() / zoom) + borderSize / 2 < imageSize.width) {
                x1 = ((int) (p.getX() / zoom));
                updateSize();
            }
            result = true;
        }
        if (top == true) {
            if ((int) (p.getY() / zoom) > y1) {
                top = false;
                bottom = true;
            } else if ((int) (p.getY() / zoom) - borderSize / 2 > 0) {
                y = ((int) (p.getY() / zoom));
                updateSize();
            }
            result = true;
        }

        if (bottom == true) {
            if ((int) (p.getY() / zoom) < y) {
                top = true;
                bottom = false;
            } else if ((int) (p.getY() / zoom) + borderSize / 2 < imageSize.width) {
                y1 = ((int) (p.getY() / zoom));
                updateSize();
            }
            result = true;
        }

        if (center == true) {
            int difX = (int) (clickPoint.x / zoom - p.x / zoom);
            int difY = (int) (clickPoint.y / zoom - p.y / zoom);
            if (x - difX - borderSize / 2 > 0 && x1 - difX + borderSize / 2 < imageSize.width) {
                x -= difX;
                x1 -= difX;
            }
            if (y - difY - borderSize / 2 > 0 && y1 - difY + borderSize / 2 < imageSize.height) {
                y -= difY;
                y1 -= difY;
            }
            clickPoint = p;
            updateSize();
            result = true;
        }
        return result;
    }
}