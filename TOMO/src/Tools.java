import ij.ImagePlus;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by thom1 on 16/03/2017.
 */
public class Tools {

    public static Integer StringToInteger(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static Float StringToFloat(String s) {
        try {
            return Float.parseFloat(s);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static ArrayList<Pixel> getPixelFromCircle(Circle circle, ImagePlus image) {
        ArrayList<Pixel> pixels = new ArrayList<>();
        for (int i = circle.getX() - circle.getWidth() / 2; i < circle.getX() + circle.getWidth() / 2; i++) {
            for (int j = circle.getY() - circle.getWidth() / 2; j < circle.getY() + circle.getWidth() / 2; j++) {
                int[] pixel = image.getPixel(i, j);
                pixels.add(new Pixel(pixel[0] / 3, i, j));
            }
        }

        for (int i = 0; i < pixels.size(); i++) {
            if ((Math.sqrt(Math.pow((pixels.get(i).getX() - circle.x), 2) +
                    Math.pow((pixels.get(i).getY() - circle.y), 2))) > circle.getWidth() / 2) {
                pixels.remove(i);
            }
        }
        return pixels;
    }

    public static ArrayList<Pixel> getPixelFromRect(Rect rect, ImagePlus image) {
        ArrayList<Pixel> pixels = new ArrayList<>();
        for (int i = rect.getX(); i < rect.getWidth() + rect.getX(); i++) {
            for (int j = rect.getY(); j < rect.getHeight() + rect.getY(); j++) {

                int[] pixel = image.getPixel(i, j);
                pixels.add(new Pixel(pixel[0] / 3, i, j));
            }
        }
        return pixels;
    }
}
