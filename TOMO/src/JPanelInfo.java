import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfWriter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by thom1 on 01/05/2017.
 */
public class JPanelInfo extends JScrollPane {

    protected Information information;
    private JPanel main;
    private JPanel panel;
    private String text;

    public JPanelInfo(String text) {
        super(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        this.text = text;
        main = new JPanel(new BorderLayout());
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        main.add(panel, BorderLayout.NORTH);
        main.add(exportButton(), BorderLayout.SOUTH);
        this.setViewportView(main);
    }

    public void add(JPanel panel) {
        this.panel.add(panel);
        revalidate();
    }

    private JPanel exportButton() {
        JPanel panel = new JPanel();
        JButton export = new JButton("exportation");
        export.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Document document = new Document(PageSize.A4);
                    PdfWriter.getInstance(document, new FileOutputStream(text + ".pdf"));
                    document.open();
                    information.exportation(document);
                    document.close();
                } catch (DocumentException e1) {
                    e1.printStackTrace();
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        });
        panel.add(export);
        return panel;
    }

}
