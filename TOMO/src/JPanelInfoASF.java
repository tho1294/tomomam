import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by thom1 on 01/05/2017.
 */
public class JPanelInfoASF extends JPanelInfo {

    private JPanel graph;

    public JPanelInfoASF(String text) {
        super(text);
        graph = new JPanel();
        add(title("ASF"));
        add(graph);
    }


    private JPanel title(String title) {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        panel.add(new JLabel(title));
        return panel;
    }

    public void setInfo(ASF info) {
        this.information = info;
        DrawGraph graphique = new DrawGraph(info.getAsf());
        graph.removeAll();
        graph.add(graphique);
    }
}
