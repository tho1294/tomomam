import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPTable;
import ij.ImagePlus;
import ij.measure.Calibration;

import java.awt.*;
import java.util.Properties;

/**
 * Created by thom1 on 06/05/2017.
 */
public class Concordance implements Information {

    private float ADIAGONAL;
    private float BDIAGONAL;
    private float CDIAGONAL;

    private Line A;
    private Line B;
    private Line C;
    private ImagePlus image;

    public Concordance(ImagePlus image, Line A, Line B, Line C) {
        this.A = A;
        this.B = B;
        this.C = C;
        this.image = image;
        calcul();
    }

    private void calcul() {
        Calibration cal = image.getCalibration();
        float width = (float) cal.pixelWidth;
        ADIAGONAL = (float) Math.sqrt(Math.pow((A.getA().x - A.getB().x), 2) + Math.pow((A.getA().y - A.getB().y), 2)) * width;
        BDIAGONAL = (float) Math.sqrt(Math.pow((B.getA().x - B.getB().x), 2) + Math.pow((B.getA().y - B.getB().y), 2)) * width;
        CDIAGONAL = (float) Math.sqrt(Math.pow((C.getA().x - C.getB().x), 2) + Math.pow((C.getA().y - C.getB().y), 2)) * width;
    }

    public float getADIAGONAL() {
        return ADIAGONAL;
    }

    public float getBDIAGONAL() {
        return BDIAGONAL;
    }

    public float getCDIAGONAL() {
        return CDIAGONAL;
    }

    @Override
    public void exportation(Document document) {
        try {
            Config config = Frame.getInstance().getConfig();
            document.addTitle("Concordance");
            document.add(new Paragraph("Concordance"));

            document.add(new Paragraph(" "));
            PdfPTable table = new PdfPTable(3);

            table.addCell(" ");
            table.addCell("Expected value (mm)");
            table.addCell("Measured value (mm)");
            table.addCell("A Diagonal 1");
            table.addCell("" + config.getADiagonal());
            table.addCell("" + ADIAGONAL);
            table.addCell("B Diagonal 2");
            table.addCell("" + config.getBDiagonal());
            table.addCell("" + BDIAGONAL);
            table.addCell("C Length");
            table.addCell("" + config.getCDiagonal());
            table.addCell("" + CDIAGONAL);
            document.add(table);

        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
}
