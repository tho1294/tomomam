import ij.ImagePlus;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by thom1 on 04/05/2017.
 */
public class JPanelMainSDNR extends JPanel implements JPanelMain {
    private JPanelImage image;
    private JPanelInfoSDNR info;

    public JPanelMainSDNR(String text) {
        super(new BorderLayout());
        int imageIndex = Frame.getInstance().getConfig().getSdnr();
        this.image = new JPanelImage(JsonController.loadSdnr(), text, imageIndex);
        this.info = new JPanelInfoSDNR(text);
        image.setJPanelMain(this);
        this.add(image, BorderLayout.CENTER);
        this.add(info, BorderLayout.EAST);
    }

    public void calcul(ArrayList<Forme> formes) {

        Circle adi100 = null;
        Circle adi80 = null;
        Circle gra100 = null;
        Circle gra80 = null;

        for (int i = 0; i < formes.size(); i++) {
            if (formes.get(i).getText().equals("glandular 100%")) {
                gra100 = (Circle) formes.get(i);
            } else if (formes.get(i).getText().equals("glandular 80%")) {
                gra80 = (Circle) formes.get(i);
            } else if (formes.get(i).getText().equals("adipose 100%")) {
                adi100 = (Circle) formes.get(i);
            } else if (formes.get(i).getText().equals("adipose 80%")) {
                adi80 = (Circle) formes.get(i);
            }
        }
        Config config = Frame.getInstance().getConfig();
        ImagePlus image = Frame.getInstance().getImage(config.getSdnr());
        SDNR sdnr = new SDNR(image, adi100, adi80, gra100, gra80);
        info.setInfo(sdnr);
        revalidate();
    }
}
