import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by thom1 on 01/05/2017.
 */
public class JPanelMenuImage extends JPanel implements ActionListener, ChangeListener {

    private JSlider slider;
    private JTextField sliderText;
    private JButton set;
    private JButton save;
    private JButton calcul;
    private JPanelImage jPanelImage;

    public JPanelMenuImage(JPanelImage jPanelImage, int imageIndex) {
        super();
        this.jPanelImage = jPanelImage;

        this.setLayout(new FlowLayout(FlowLayout.LEFT));
        save = new JButton("Sauvegarder");
        calcul = new JButton("Calculer");
        slider = new JSlider(0, Frame.getInstance().getNbImage() - 1, 0);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.setValue(imageIndex);

        set = new JButton("Aller");
        sliderText = new JTextField(slider.getValue() + "");

        save.addActionListener(this);
        calcul.addActionListener(this);
        set.addActionListener(this);
        slider.addChangeListener(this);

        this.add(save);
        this.add(calcul);
        this.add(new JLabel(" Changer d'image "));
        this.add(slider);
        this.add(new JLabel(" Aller à l'image : "));
        this.add(sliderText);
        this.add(set);

        addBorder();
    }

    private void addBorder() {
        this.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.BLACK));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == set) {
            Integer value = Tools.StringToInteger(sliderText.getText());
            if (value != null) {
                slider.setValue(value);
                sliderText.setText(slider.getValue() + "");
                jPanelImage.setImage(value);
            } else {
                System.out.println("error");
            }
        } else if (e.getSource() == calcul) {
            jPanelImage.calcul();
        } else if (e.getSource() == save) {
            jPanelImage.save();
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        int value = slider.getValue();
        System.out.println("change" + value);
        jPanelImage.setImage(value);
        sliderText.setText(value + "");
        revalidate();
    }
}
