import ij.ImagePlus;
import ij.ImageStack;
import ij.io.FileInfo;
import ij.io.FileOpener;
import ij.io.Opener;
import ij.plugin.DICOM;
import ij.process.ImageProcessor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by thom1 on 15/02/2017.
 */
public class OpenListener implements ActionListener, PropertyChangeListener {
    private Frame frame;
    private ProgressMonitor progressMonitor;
    private Task task;
    private File[] selectedFiles;

    public OpenListener(Frame frame) {
        this.frame = frame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser fc = new JFileChooser();
        fc.setMultiSelectionEnabled(true);
        if (fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
            selectedFiles = fc.getSelectedFiles();

            progressMonitor = new ProgressMonitor(frame,
                    "Chargement des images",
                    "", 0, selectedFiles.length - 1);
            progressMonitor.setProgress(0);

            frame.setEnabled(false);

            task = new Task(selectedFiles);
            task.addPropertyChangeListener(this);
            task.execute();
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("progress" == evt.getPropertyName()) {
            int progress = (Integer) evt.getNewValue();
            String message = "Completed : " + progress + "/" + (selectedFiles.length - 1);
            progressMonitor.setNote(message);
            progressMonitor.setProgress(progress);
        }
        if (progressMonitor.isCanceled() || task.isDone()) {
            task.cancel(true);
            frame.setEnabled(true);
            frame.setVisible(true);
        }
    }

    class Task extends SwingWorker<Void, Void> {
        private File[] selectedFiles;

        public Task(File[] selectedFiles) {
            this.selectedFiles = selectedFiles;
        }

        @Override
        public Void doInBackground() {
            ArrayList<ImagePlus> images = new ArrayList<>();
            for (int i = 0; i < selectedFiles.length && !isCancelled(); i++) {

                ImagePlus imagePlus = new ImagePlus(selectedFiles[i].getPath());
                if (imagePlus.getStack().getSize() == 1) {
                    images.add(imagePlus);
                } else {
                    ImageStack imgstack = imagePlus.getStack();
                    for (int j = 0; j < imgstack.size() && !isCancelled(); j++) {
                        ImageProcessor ip = imgstack.getProcessor(j + 1);
                        ImagePlus imgp = new ImagePlus();
                        imgp.setProcessor(ip);
                        images.add(imgp);
                    }
                }
                setProgress(i);
            }

            ImagePlus[] imgs = new ImagePlus[images.size()];
            for (int i = 0; i < images.size(); i++) {
                imgs[i] = images.get(i);
            }
            Frame.getInstance().setFiles(imgs);
            return null;
        }

        @Override
        public void done() {

        }
    }
}
