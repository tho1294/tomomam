import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by thom1 on 01/05/2017.
 */
public interface JPanelMain {
    void calcul(ArrayList<Forme> formes);
}
