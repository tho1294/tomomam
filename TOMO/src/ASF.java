import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPTable;
import ij.ImagePlus;

import java.util.ArrayList;

/**
 * Created by thom1 on 04/05/2017.
 */
public class ASF implements Information {


    private ArrayList<Float> PV_MAX_STEEL_BALL;
    private ArrayList<Float> PV_MAX_STEEL_BALL_MINUS_PV_BACKGROUND;
    private ArrayList<Float> asf;

    private int firstPositon;
    private float PV_BACKGROUND;
    private float SD_BACKGROUND;
    private float START_PV_MAX_STEEL_BALL_MINUS_PV_BACKGROUND;

    private ArrayList<ImagePlus> images;

    private Rect rect;
    private Rect background;

    public ASF(ArrayList<ImagePlus> images, Rect zone, Rect background) {
        this.images = images;
        this.rect = zone;
        this.background = background;
        this.firstPositon = Frame.getInstance().getConfig().getAsfMin();

        PV_MAX_STEEL_BALL = new ArrayList<>();
        PV_MAX_STEEL_BALL_MINUS_PV_BACKGROUND = new ArrayList<>();
        asf = new ArrayList<>();

        calcul();
    }

    private void calcul() {
        ArrayList<Pixel> pixels = Tools.getPixelFromRect(background, images.get(0));
        PV_BACKGROUND = Calcul.moyenne(pixels);
        SD_BACKGROUND = Calcul.StdDev(pixels);
        START_PV_MAX_STEEL_BALL_MINUS_PV_BACKGROUND = Calcul.max(Tools.getPixelFromRect(rect, images.get(0))) - PV_BACKGROUND;

        for (int i = 0; i < images.size(); i++) {
            pixels = Tools.getPixelFromRect(rect, images.get(i));
            PV_MAX_STEEL_BALL.add(Calcul.max(pixels));
            PV_MAX_STEEL_BALL_MINUS_PV_BACKGROUND.add(PV_MAX_STEEL_BALL.get(i) - PV_BACKGROUND);
            asf.add(PV_MAX_STEEL_BALL_MINUS_PV_BACKGROUND.get(i) / START_PV_MAX_STEEL_BALL_MINUS_PV_BACKGROUND);
        }
    }

    public ArrayList<Float> getPV_MAX_STEEL_BALL() {
        return PV_MAX_STEEL_BALL;
    }

    public ArrayList<Float> getPV_MAX_STEEL_BALL_MINUS_PV_BACKGROUND() {
        return PV_MAX_STEEL_BALL_MINUS_PV_BACKGROUND;
    }

    public ArrayList<Float> getAsf() {
        return asf;
    }

    public int getFirstPositon() {
        return firstPositon;
    }

    public float getPV_BACKGROUND() {
        return PV_BACKGROUND;
    }

    public float getSD_BACKGROUND() {
        return SD_BACKGROUND;
    }

    public float getSTART_PV_MAX_STEEL_BALL_MINUS_PV_BACKGROUND() {
        return START_PV_MAX_STEEL_BALL_MINUS_PV_BACKGROUND;
    }

    @Override
    public void exportation(Document document) {
        try {

            document.addTitle("ASF");
            document.add(new Paragraph("ASF"));

            document.add(new Paragraph(" "));
            PdfPTable table = new PdfPTable(7);

            table.addCell("Vertical location from z=+" + firstPositon + "mm(mm)");
            table.addCell("PV max steel ball");
            table.addCell("PV background");
            table.addCell("SD background");
            table.addCell("PVmax steel ball-PVbackground");
            table.addCell("PVmax steel ball 30mm-Pvbackground 30mm");
            table.addCell("ASF");

            for (int i = 0; i < PV_MAX_STEEL_BALL.size(); i++) {
                table.addCell("" + i);
                table.addCell("" + PV_MAX_STEEL_BALL.get(i));
                table.addCell("" + PV_BACKGROUND);
                table.addCell("" + SD_BACKGROUND);
                table.addCell("" + PV_MAX_STEEL_BALL_MINUS_PV_BACKGROUND.get(i));
                table.addCell("" + START_PV_MAX_STEEL_BALL_MINUS_PV_BACKGROUND);
                table.addCell("" + asf.get(i));
            }

            document.add(table);


        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }
}
