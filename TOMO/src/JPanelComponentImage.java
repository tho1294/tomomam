import ij.ImagePlus;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

/**
 * Created by thom1 on 02/05/2017.
 */
public class JPanelComponentImage extends JPanel implements MouseWheelListener, MouseListener, MouseMotionListener {
    private ImagePlus currentImage;
    private float zoom = 0.2f;
    private JScrollPaneImage jScrollPaneImage;

    private ArrayList<Forme> formes;

    public JPanelComponentImage(JScrollPaneImage jScrollPaneImage, ArrayList<Forme> formes, int imageIndex) {
        super();
        this.formes = formes;
        this.jScrollPaneImage = jScrollPaneImage;
        this.currentImage = Frame.getInstance().getImage(imageIndex);
        this.addMouseWheelListener(this);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    protected void paintComponent(Graphics gd) {
        super.paintComponent(gd);
        Graphics2D g = (Graphics2D) gd;
        AffineTransform t = new AffineTransform();

        t.scale(zoom, zoom);

        g.drawImage(currentImage.getBufferedImage(), t, null);
        this.setPreferredSize(new Dimension((int) (currentImage.getHeight() * zoom), (int) (currentImage.getHeight() * zoom)));

        for (int i = 0; i < formes.size(); i++) {
            formes.get(i).draw(g, zoom);
        }

        g.dispose();
    }

    public void zoomOut(Point point) {
        zoom *= 0.9;
        Point pos = jScrollPaneImage.getViewport().getViewPosition();

        int newX = (int) (point.x * (0.9f - 1f) + 0.9f * pos.x);
        int newY = (int) (point.y * (0.9f - 1f) + 0.9f * pos.y);
        jScrollPaneImage.getViewport().setViewPosition(new Point(newX, newY));

        //this.revalidate();
        this.repaint();
    }

    public void zoomIn(Point point) {
        zoom *= 1.1;
        Point pos = jScrollPaneImage.getViewport().getViewPosition();

        int newX = (int) (point.x * (1.1f - 1f) + 1.1f * pos.x);
        int newY = (int) (point.y * (1.1f - 1f) + 1.1f * pos.y);
        jScrollPaneImage.getViewport().setViewPosition(new Point(newX, newY));

        //  this.revalidate();
        this.repaint();
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getWheelRotation() > 0) {
            zoomOut(e.getPoint());
        } else {
            zoomIn(e.getPoint());
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        for (int i = 0; i < formes.size(); i++) {
            if (formes.get(i).mouseDragged(e.getPoint(), zoom)) {
                break;
            }
        }
        repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        for (int i = 0; i < formes.size(); i++) {
            formes.get(i).mousePressed(e.getPoint(), zoom);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        for (int i = 0; i < formes.size(); i++) {
            formes.get(i).mouseReleased();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public void setImage(int index) {
        currentImage = Frame.getInstance().getImage(index);
        repaint();
    }
}
