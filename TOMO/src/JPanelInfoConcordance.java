import javax.swing.*;
import java.awt.*;

/**
 * Created by thom1 on 06/05/2017.
 */
public class JPanelInfoConcordance extends JPanelInfo {

    private JLabel ADIAGONAL;
    private JLabel BDIAGONAL;
    private JLabel CDIAGONAL;

    private JLabel EADIAGONAL;
    private JLabel EBDIAGONAL;
    private JLabel ECDIAGONAL;

    private Config config;

    public JPanelInfoConcordance(String text) {
        super(text);
        config = Frame.getInstance().getConfig();
        add(title("Concordance"));
        add(APanel());
        add(BPanel());
        add(CPanel());
    }


    private JPanel title(String title) {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        panel.add(new JLabel(title));
        return panel;
    }

    private JPanel APanel() {
        JPanel APanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        APanel.add(new JLabel("diagonal A"));
        APanel.add(new JLabel("esperée : "));
        EADIAGONAL = new JLabel(config.getADiagonal() + "");
        APanel.add(EADIAGONAL);
        APanel.add(new JLabel("mesurée : "));
        ADIAGONAL = new JLabel(" ");
        APanel.add(ADIAGONAL);

        return APanel;
    }

    private JPanel BPanel() {
        JPanel BPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        BPanel.add(new JLabel("diagonal B"));
        BPanel.add(new JLabel("esperée : "));
        EBDIAGONAL = new JLabel(config.getBDiagonal() + "");
        BPanel.add(EBDIAGONAL);
        BPanel.add(new JLabel("mesurée : "));
        BDIAGONAL = new JLabel(" ");
        BPanel.add(BDIAGONAL);

        return BPanel;
    }

    private JPanel CPanel() {
        JPanel CPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        CPanel.add(new JLabel("diagonal C"));
        CPanel.add(new JLabel("esperée : "));
        ECDIAGONAL = new JLabel(config.getCDiagonal() + "");
        CPanel.add(ECDIAGONAL);
        CPanel.add(new JLabel("mesurée : "));
        CDIAGONAL = new JLabel(" ");
        CPanel.add(CDIAGONAL);
        return CPanel;
    }

    public void setInfo(Concordance info) {
        this.information = info;

        ADIAGONAL.setText(info.getADIAGONAL() + "");
        BDIAGONAL.setText((info.getBDIAGONAL() + ""));
        CDIAGONAL.setText(info.getCDIAGONAL() + "");

        this.config = Frame.getInstance().getConfig();

        EADIAGONAL.setText(config.getADiagonal() + "");
        EBDIAGONAL.setText((config.getBDiagonal() + ""));
        ECDIAGONAL.setText(config.getCDiagonal() + "");

    }
}
