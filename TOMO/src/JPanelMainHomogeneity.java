import ij.ImagePlus;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by thom1 on 06/05/2017.
 */
public class JPanelMainHomogeneity extends JPanel implements JPanelMain {


    private JPanelImage image;
    private JPanelInfoHomogeneity info;

    public JPanelMainHomogeneity(String text) {
        super(new BorderLayout());
        int imageIndex = Frame.getInstance().getConfig().getHomogeneity();
        this.image = new JPanelImage(JsonController.loadHomogeneity(), text, imageIndex);
        this.info = new JPanelInfoHomogeneity(text);
        image.setJPanelMain(this);
        this.add(image, BorderLayout.CENTER);
        this.add(info, BorderLayout.EAST);
    }

    @Override
    public void calcul(ArrayList<Forme> formes) {
        Rect ref = null;
        Rect zoneA = null;
        Rect zoneB = null;
        Rect zoneC = null;


        for (int i = 0; i < formes.size(); i++) {
            if (formes.get(i).getText().equals("ref")) {
                ref = (Rect) formes.get(i);
            } else if (formes.get(i).getText().equals("zone A")) {
                zoneA = (Rect) formes.get(i);
            } else if (formes.get(i).getText().equals("zone B")) {
                zoneB = (Rect) formes.get(i);
            } else if (formes.get(i).getText().equals("zone C")) {
                zoneC = (Rect) formes.get(i);
            }
        }
        Config config = Frame.getInstance().getConfig();
        ImagePlus images = Frame.getInstance().getImage(config.getHomogeneity());


        Homogeneity homogeneity = new Homogeneity(images, ref, zoneA, zoneB, zoneC);
        info.setInfo(homogeneity);
        revalidate();
    }
}
