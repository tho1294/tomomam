import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfTable;
import ij.ImagePlus;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by thom1 on 03/05/2017.
 */
public class SDNR implements Information {
    private Circle adipose100;
    private Circle adipose80;
    private Circle glandular100;
    private Circle glandular80;
    private ImagePlus image;

    private float a100PVMEAN;
    private float a100SD;
    private float a80PVMEAN;
    private float a80SD;
    private float g100PVMEAN;
    private float g100SD;
    private float g80PVMEAN;
    private float g80SD;

    private float A_PV100_MINUS_PV80;
    private float A_SD2100_PLUS_SD280_DIV_2;
    private float A_RACINE_SD2100_PLUS_SD280_DIV_2;
    private float A_SDNR;

    private float G_PV100_MINUS_PV80;
    private float G_SD2100_PLUS_SD280_DIV_2;
    private float G_RACINE_SD2100_PLUS_SD280_DIV_2;
    private float G_SDNR;

    public SDNR(ImagePlus image, Circle adipose100, Circle adipose80, Circle glandular100, Circle glandular80) {
        this.adipose100 = adipose100;
        this.adipose80 = adipose80;
        this.glandular100 = glandular100;
        this.glandular80 = glandular80;
        this.image = image;
        calcul();
    }

    public void calcul() {
        ArrayList<Pixel> pa100 = Tools.getPixelFromCircle(adipose100, image);
        ArrayList<Pixel> pa80 = Tools.getPixelFromCircle(adipose80, image);
        ArrayList<Pixel> pg100 = Tools.getPixelFromCircle(glandular100, image);
        ArrayList<Pixel> pg80 = Tools.getPixelFromCircle(glandular80, image);

        a100PVMEAN = Calcul.moyenne(pa100);
        a80PVMEAN = Calcul.moyenne(pa80);
        g100PVMEAN = Calcul.moyenne(pg100);
        g80PVMEAN = Calcul.moyenne(pg80);

        a100SD = Calcul.StdDev(pa100);
        a80SD = Calcul.StdDev(pa80);
        g100SD = Calcul.StdDev(pg100);
        g80SD = Calcul.StdDev(pg80);

        A_PV100_MINUS_PV80 = a100PVMEAN - a80PVMEAN;
        A_SD2100_PLUS_SD280_DIV_2 = ((a100SD * a100SD) + (a80SD * a80SD)) / 2;
        A_RACINE_SD2100_PLUS_SD280_DIV_2 = (float) Math.sqrt(A_SD2100_PLUS_SD280_DIV_2);
        A_SDNR = -A_PV100_MINUS_PV80 / A_RACINE_SD2100_PLUS_SD280_DIV_2;

        G_PV100_MINUS_PV80 = g100PVMEAN - g80PVMEAN;
        G_SD2100_PLUS_SD280_DIV_2 = ((g100SD * g100SD) + (g80SD * g80SD)) / 2;
        G_RACINE_SD2100_PLUS_SD280_DIV_2 = (float) Math.sqrt(G_SD2100_PLUS_SD280_DIV_2);
        G_SDNR = -G_PV100_MINUS_PV80 / G_RACINE_SD2100_PLUS_SD280_DIV_2;
    }

    public float getA100PVMEAN() {
        return a100PVMEAN;
    }

    public float getA100SD() {
        return a100SD;
    }

    public float getA80PVMEAN() {
        return a80PVMEAN;
    }

    public float getA80SD() {
        return a80SD;
    }

    public float getG100PVMEAN() {
        return g100PVMEAN;
    }

    public float getG100SD() {
        return g100SD;
    }

    public float getG80PVMEAN() {
        return g80PVMEAN;
    }

    public float getG80SD() {
        return g80SD;
    }

    public float getA_PV100_MINUS_PV80() {
        return A_PV100_MINUS_PV80;
    }

    public float getA_SD2100_PLUS_SD280_DIV_2() {
        return A_SD2100_PLUS_SD280_DIV_2;
    }

    public float getA_RACINE_SD2100_PLUS_SD280_DIV_2() {
        return A_RACINE_SD2100_PLUS_SD280_DIV_2;
    }

    public float getA_SDNR() {
        return A_SDNR;
    }

    public float getG_PV100_MINUS_PV80() {
        return G_PV100_MINUS_PV80;
    }

    public float getG_SD2100_PLUS_SD280_DIV_2() {
        return G_SD2100_PLUS_SD280_DIV_2;
    }

    public float getG_RACINE_SD2100_PLUS_SD280_DIV_2() {
        return G_RACINE_SD2100_PLUS_SD280_DIV_2;
    }

    public float getG_SDNR() {
        return G_SDNR;
    }

    @Override
    public void exportation(Document document) {
        try {

            document.addTitle("SDNR");
            document.add(new Paragraph("SDNR"));

            document.add(new Paragraph(" "));
            PdfPTable table = new PdfPTable(3);

            table.addCell("Sphere composition");
            table.addCell("PVmean");
            table.addCell("SD");
            table.addCell("100% adipose(dark)");
            table.addCell("" + a100PVMEAN);
            table.addCell("" + a100SD);
            table.addCell("80% adipose");
            table.addCell("" + a80PVMEAN);
            table.addCell("" + a80SD);
            table.addCell("80% glandular");
            table.addCell("" + g100PVMEAN);
            table.addCell("" + g100SD);
            table.addCell("100% glandular (white)");
            table.addCell("" + g80PVMEAN);
            table.addCell("" + g80SD);

            document.add(table);

            PdfPTable tableA = new PdfPTable(3);
            PdfPCell cellA = new PdfPCell(new Phrase("Adipose"));
            cellA.setRowspan(4);
            tableA.addCell(cellA);

            tableA.addCell("PV 100% - PV 80%");
            tableA.addCell("" + A_PV100_MINUS_PV80);
            tableA.addCell("(SD² 100% + SD² 80%)/2");
            tableA.addCell("" + A_SD2100_PLUS_SD280_DIV_2);
            tableA.addCell("RACINE((SD² 100% + SD² 80%)/2)");
            tableA.addCell("" + A_RACINE_SD2100_PLUS_SD280_DIV_2);
            tableA.addCell("SDNR (adipose masses)");
            tableA.addCell("" + A_SDNR);

            document.add(new Paragraph(" "));
            document.add(tableA);

            PdfPTable tableG = new PdfPTable(3);
            PdfPCell cellG = new PdfPCell(new Phrase("Glandular"));
            cellG.setRowspan(4);
            tableG.addCell(cellG);

            tableG.addCell("PV 100% - PV 80%");
            tableG.addCell("" + G_PV100_MINUS_PV80);
            tableG.addCell("(SD² 100% + SD² 80%)/2");
            tableG.addCell("" + G_SD2100_PLUS_SD280_DIV_2);
            tableG.addCell("RACINE((SD² 100% + SD² 80%)/2)");
            tableG.addCell("" + G_RACINE_SD2100_PLUS_SD280_DIV_2);
            tableG.addCell("SDNR (glandular masses)");
            tableG.addCell("" + G_SDNR);

            document.add(new Paragraph(" "));
            document.add(tableG);

        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }
}
