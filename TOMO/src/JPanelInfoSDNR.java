import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfWriter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by thom1 on 01/05/2017.
 */
public class JPanelInfoSDNR extends JPanelInfo {

    private JLabel la100PV;
    private JLabel la80PV;
    private JLabel lg100PV;
    private JLabel lg80PV;

    private JLabel la100SD;
    private JLabel la80SD;
    private JLabel lg100SD;
    private JLabel lg80SD;

    private JLabel A_PV100_MINUS_PV80;
    private JLabel A_SD2100_PLUS_SD280_DIV_2;
    private JLabel A_RACINE_SD2100_PLUS_SD280_DIV_2;
    private JLabel A_SDNR;

    private JLabel G_PV100_MINUS_PV80;
    private JLabel G_SD2100_PLUS_SD280_DIV_2;
    private JLabel G_RACINE_SD2100_PLUS_SD280_DIV_2;
    private JLabel G_SDNR;


    public JPanelInfoSDNR(String text) {
        super(text);
        add(title("SDNR"));
        add(sphereComposition());
        add(adipose());
        add(glandular());
        this.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.BLACK));
    }

    public void setInfo(SDNR sdnr) {
        this.information = sdnr;
        la100PV.setText(sdnr.getA100PVMEAN() + "");
        la80PV.setText(sdnr.getA80PVMEAN() + "");
        lg100PV.setText(sdnr.getG100PVMEAN() + "");
        lg80PV.setText(sdnr.getG80PVMEAN() + "");

        la100SD.setText(sdnr.getA100SD() + "");
        la80SD.setText(sdnr.getA80SD() + "");
        lg100SD.setText(sdnr.getG100SD() + "");
        lg80SD.setText(sdnr.getG80SD() + "");

        A_PV100_MINUS_PV80.setText(sdnr.getA_PV100_MINUS_PV80() + "");
        A_SD2100_PLUS_SD280_DIV_2.setText(sdnr.getA_SD2100_PLUS_SD280_DIV_2() + "");
        A_RACINE_SD2100_PLUS_SD280_DIV_2.setText(sdnr.getA_RACINE_SD2100_PLUS_SD280_DIV_2() + "");
        A_SDNR.setText(sdnr.getA_SDNR() + "");

        G_PV100_MINUS_PV80.setText(sdnr.getG_PV100_MINUS_PV80() + "");
        G_SD2100_PLUS_SD280_DIV_2.setText(sdnr.getG_SD2100_PLUS_SD280_DIV_2() + "");
        G_RACINE_SD2100_PLUS_SD280_DIV_2.setText(sdnr.getG_RACINE_SD2100_PLUS_SD280_DIV_2() + "");
        G_SDNR.setText(sdnr.getG_SDNR() + "");

    }


    private JPanel title(String title) {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        panel.add(new JLabel(title));
        return panel;
    }

    private JPanel sphereComposition() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JPanel title = new JPanel(new FlowLayout(FlowLayout.CENTER));
        title.add(new JLabel("Sphere composition"));

        JPanel panel100A = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel100A.add(new JLabel("100% adipose(dark)"));
        panel100A.add(new JLabel("PVMean : "));
        la100PV = new JLabel(" ");
        panel100A.add(la100PV);
        panel100A.add(new JLabel("SD : "));
        la100SD = new JLabel(" ");
        panel100A.add(la100SD);

        JPanel panel80A = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel80A.add(new JLabel("80% adipose"));
        panel80A.add(new JLabel("PVMean : "));
        la80PV = new JLabel(" ");
        panel80A.add(la80PV);
        panel80A.add(new JLabel("SD : "));
        la80SD = new JLabel(" ");
        panel80A.add(la80SD);

        JPanel panel100G = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel100G.add(new JLabel("100% glandular (white)"));
        panel100G.add(new JLabel("PVMean : "));
        lg100PV = new JLabel(" ");
        panel100G.add(lg100PV);
        panel100G.add(new JLabel("SD : "));
        lg100SD = new JLabel(" ");
        panel100G.add(lg100SD);

        JPanel panel80G = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel80G.add(new JLabel("80% glandular"));
        panel80G.add(new JLabel("PVMean : "));
        lg80PV = new JLabel(" ");
        panel80G.add(lg80PV);
        panel80G.add(new JLabel("SD : "));
        lg80SD = new JLabel(" ");
        panel80G.add(lg80SD);

        panel.add(title);
        panel.add(panel100A);
        panel.add(panel80A);
        panel.add(panel100G);
        panel.add(panel80G);
        panel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        return panel;
    }

    public JPanel adipose() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JPanel title = new JPanel(new FlowLayout(FlowLayout.CENTER));
        title.add(new JLabel("Adipose"));

        JPanel panel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel1.add(new JLabel("PV 100% - PV 80%"));
        A_PV100_MINUS_PV80 = new JLabel(" ");
        panel1.add(A_PV100_MINUS_PV80);

        JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel2.add(new JLabel("(SD² 100% + SD² 80%)/2"));
        A_SD2100_PLUS_SD280_DIV_2 = new JLabel(" ");
        panel2.add(A_SD2100_PLUS_SD280_DIV_2);

        JPanel panel3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel3.add(new JLabel("RACINE((SD² 100% + SD² 80%)/2)"));
        A_RACINE_SD2100_PLUS_SD280_DIV_2 = new JLabel(" ");
        panel3.add(A_RACINE_SD2100_PLUS_SD280_DIV_2);

        JPanel panel4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel4.add(new JLabel("SDNR (adipose masses)"));
        A_SDNR = new JLabel(" ");
        panel4.add(A_SDNR);

        panel.add(title);
        panel.add(panel1);
        panel.add(panel2);
        panel.add(panel3);
        panel.add(panel4);
        panel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        return panel;
    }

    public JPanel glandular() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JPanel title = new JPanel(new FlowLayout(FlowLayout.CENTER));
        title.add(new JLabel("Glandular"));

        JPanel panel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel1.add(new JLabel("PV 100% - PV 80%"));
        G_PV100_MINUS_PV80 = new JLabel(" ");
        panel1.add(G_PV100_MINUS_PV80);

        JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel2.add(new JLabel("(SD² 100% + SD² 80%)/2"));
        G_SD2100_PLUS_SD280_DIV_2 = new JLabel(" ");
        panel2.add(G_SD2100_PLUS_SD280_DIV_2);

        JPanel panel3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel3.add(new JLabel("RACINE((SD² 100% + SD² 80%)/2)"));
        G_RACINE_SD2100_PLUS_SD280_DIV_2 = new JLabel(" ");
        panel3.add(G_RACINE_SD2100_PLUS_SD280_DIV_2);

        JPanel panel4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel4.add(new JLabel("SDNR (adipose masses)"));
        G_SDNR = new JLabel(" ");
        panel4.add(G_SDNR);

        panel.add(title);
        panel.add(panel1);
        panel.add(panel2);
        panel.add(panel3);
        panel.add(panel4);
        panel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        return panel;
    }


}




